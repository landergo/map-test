import React from 'react';

import {
  Container,
  TypeTitle,
  TypeDescription,
  RequestButton,
  RequestButtonText,
  Avatar,
  AvatarContainer,
  HeaderContainer,
} from './styles';

const Details = ({from, to}) => {
  return (
    <Container>
      <HeaderContainer>
        <AvatarContainer>
          <Avatar>GS</Avatar>
        </AvatarContainer>
        <TypeTitle>Gregory Smith</TypeTitle>
        <AvatarContainer>
          <Avatar>A</Avatar>
        </AvatarContainer>
        <AvatarContainer>
          <Avatar>B</Avatar>
        </AvatarContainer>
      </HeaderContainer>

      <TypeDescription>{`from: ${from}`}</TypeDescription>
      <TypeDescription>{`to: ${to}`}</TypeDescription>

      <RequestButton onPress={() => {}}>
        <RequestButtonText>Request car</RequestButtonText>
      </RequestButton>
    </Container>
  );
};

export default Details;
