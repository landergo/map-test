import React from 'react';
import {View, SafeAreaView} from 'react-native';
import styled from 'styled-components/native';

const ScreenContainer = ({
  containerComponent = 'SafeAreaView',
  children,
  scroll = false,
  testID,
}) => (
  <Container
    testID={testID}
    as={containerComponent === 'SafeAreaView' ? SafeAreaView : View}>
    {<InnerWithoutScrolling>{children}</InnerWithoutScrolling>}
  </Container>
);

const Container = styled.View`
  background: ${({theme}) => theme.backgroundColors.primary};
  flex: 1;
`;

const InnerWithoutScrolling = styled.View`
  flex: 1;
  padding-top: ${({theme}) => `${theme.safeArea.top}px`};
  padding-bottom: ${({theme}) => `${theme.safeArea.bottom}px`};
  padding-left: ${({theme}) => `${theme.safeArea.left}px`};
  padding-right: ${({theme}) => `${theme.safeArea.right}px`};
`;

export default ScreenContainer;
