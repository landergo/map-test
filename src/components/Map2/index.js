import React, {Fragment, useEffect, useReducer, useRef} from 'react';
import {View, Text} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import markerImage from './../../assets/ic_Pin.png';
import Search from '../Search';
import Directions from '../Directions';
import Details from '../Details';

import {
  Back,
  LocationBox,
  LocationText,
  LocationTimeBox,
  LocationTimeText,
  LocationTimeTextSmall,
} from './styles';

const geoCodingResponse = {
  "plus_code" : {
     "compound_code" : "F948+HX Se, São Paulo - SP, Brazil",
     "global_code" : "588MF948+HX"
  },
  "results" : [
     {
      "address_components" : [
       {
        "long_name" : "702-722",
        "short_name" : "702-722",
        "types" : [ "street_number" ]
       },
       {
        "long_name" : "25 de Março",
        "short_name" : "25 de Março",
        "types" : [ "route" ]
       },
       {
        "long_name" : "Centro Histórico de São Paulo",
        "short_name" : "Centro Histórico de São Paulo",
        "types" : [ "political", "sublocality", "sublocality_level_1" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       },
       {
        "long_name" : "01021-100",
        "short_name" : "01021-100",
        "types" : [ "postal_code" ]
       }
      ],
      "formatted_address" : "25 de Março, 702-722 - Centro Histórico de São Paulo, São Paulo - SP, 01021-100, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5434569,
           "lng" : -46.6324722
        },
        "southwest" : {
           "lat" : -23.5436238,
           "lng" : -46.6325199
        }
       },
       "location" : {
        "lat" : -23.5435404,
        "lng" : -46.632496
       },
       "location_type" : "GEOMETRIC_CENTER",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5421913697085,
           "lng" : -46.6311470697085
        },
        "southwest" : {
           "lat" : -23.5448893302915,
           "lng" : -46.6338450302915
        }
       }
      },
      "place_id" : "ChIJfc_Y8VVYzpQRMIxCMahoHM0",
      "types" : [ "route" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "01023-070",
        "short_name" : "01023-070",
        "types" : [ "postal_code" ]
       },
       {
        "long_name" : "Centro Histórico de São Paulo",
        "short_name" : "Centro Histórico de São Paulo",
        "types" : [ "political", "sublocality", "sublocality_level_1" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "Centro Histórico de São Paulo, São Paulo - State of São Paulo, 01023-070, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5431741,
           "lng" : -46.631913
        },
        "southwest" : {
           "lat" : -23.5437998,
           "lng" : -46.632541
        }
       },
       "location" : {
        "lat" : -23.5434451,
        "lng" : -46.6323735
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5421379697085,
           "lng" : -46.6308780197085
        },
        "southwest" : {
           "lat" : -23.5448359302915,
           "lng" : -46.6335759802915
        }
       }
      },
      "place_id" : "ChIJk_uo9lVYzpQRn-ueBqZC0w4",
      "types" : [ "postal_code" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "01022",
        "short_name" : "01022",
        "types" : [ "postal_code", "postal_code_prefix" ]
       },
       {
        "long_name" : "Centro Histórico de São Paulo",
        "short_name" : "Centro Histórico de São Paulo",
        "types" : [ "political", "sublocality", "sublocality_level_1" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "Centro Histórico de São Paulo, São Paulo - State of São Paulo, 01022, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5430339,
           "lng" : -46.6285299
        },
        "southwest" : {
           "lat" : -23.5461147,
           "lng" : -46.6329023
        }
       },
       "location" : {
        "lat" : -23.5449116,
        "lng" : -46.6310166
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5430339,
           "lng" : -46.6285299
        },
        "southwest" : {
           "lat" : -23.5461147,
           "lng" : -46.6329023
        }
       }
      },
      "place_id" : "ChIJZYH1n_9YzpQRtX6uZr1dbIU",
      "types" : [ "postal_code", "postal_code_prefix" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "Centro Histórico de São Paulo",
        "short_name" : "Centro Histórico de São Paulo",
        "types" : [ "political", "sublocality", "sublocality_level_1" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "Centro Histórico de São Paulo, São Paulo - State of São Paulo, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5347762,
           "lng" : -46.6267399
        },
        "southwest" : {
           "lat" : -23.5508965,
           "lng" : -46.6389979
        }
       },
       "location" : {
        "lat" : -23.5406338,
        "lng" : -46.6320967
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5347762,
           "lng" : -46.6267399
        },
        "southwest" : {
           "lat" : -23.5508965,
           "lng" : -46.6389979
        }
       }
      },
      "place_id" : "ChIJKV9sClZYzpQRekug1uZ38e8",
      "types" : [ "political", "sublocality", "sublocality_level_1" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "Se",
        "short_name" : "Se",
        "types" : [ "administrative_area_level_4", "political" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "Se, São Paulo - SP, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5358761,
           "lng" : -46.6249121
        },
        "southwest" : {
           "lat" : -23.5567494,
           "lng" : -46.6387981
        }
       },
       "location" : {
        "lat" : -23.544917,
        "lng" : -46.6309586
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5358761,
           "lng" : -46.6249121
        },
        "southwest" : {
           "lat" : -23.5567494,
           "lng" : -46.6387981
        }
       }
      },
      "place_id" : "ChIJ_R2V9f9YzpQRN8HFcPcLOB4",
      "types" : [ "administrative_area_level_4", "political" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "locality", "political" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "São Paulo, State of São Paulo, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.3566039,
           "lng" : -46.3650844
        },
        "southwest" : {
           "lat" : -24.0082209,
           "lng" : -46.825514
        }
       },
       "location" : {
        "lat" : -23.5557714,
        "lng" : -46.6395571
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.3566039,
           "lng" : -46.3650844
        },
        "southwest" : {
           "lat" : -24.0082209,
           "lng" : -46.825514
        }
       }
      },
      "place_id" : "ChIJ0WGkg4FEzpQRrlsz_whLqZs",
      "types" : [ "locality", "political" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "São Paulo - State of São Paulo, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.356293,
           "lng" : -46.3650838
        },
        "southwest" : {
           "lat" : -24.0084309,
           "lng" : -46.826199
        }
       },
       "location" : {
        "lat" : -23.5557867,
        "lng" : -46.6395475
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -23.356293,
           "lng" : -46.3650838
        },
        "southwest" : {
           "lat" : -24.0084309,
           "lng" : -46.826199
        }
       }
      },
      "place_id" : "ChIJ9cXwmIFEzpQR7-ebZCySXMo",
      "types" : [ "administrative_area_level_2", "political" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "State of São Paulo, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -19.7796559,
           "lng" : -44.1613651
        },
        "southwest" : {
           "lat" : -25.3579997,
           "lng" : -53.1101115
        }
       },
       "location" : {
        "lat" : -21.2922457,
        "lng" : -50.3428431
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : -19.7796559,
           "lng" : -44.1613651
        },
        "southwest" : {
           "lat" : -25.3579997,
           "lng" : -53.1101115
        }
       }
      },
      "place_id" : "ChIJrVgvRn1ZzpQRF3x74eJBUh4",
      "types" : [ "administrative_area_level_1", "political" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : 5.2717863,
           "lng" : -28.650543
        },
        "southwest" : {
           "lat" : -34.0891,
           "lng" : -73.9828169
        }
       },
       "location" : {
        "lat" : -14.235004,
        "lng" : -51.92528
       },
       "location_type" : "APPROXIMATE",
       "viewport" : {
        "northeast" : {
           "lat" : 5.2717863,
           "lng" : -28.650543
        },
        "southwest" : {
           "lat" : -34.0891,
           "lng" : -73.9828169
        }
       }
      },
      "place_id" : "ChIJzyjM68dZnAARYz4p8gYVWik",
      "types" : [ "country", "political" ]
     },
     {
      "address_components" : [
       {
        "long_name" : "F948+HX",
        "short_name" : "F948+HX",
        "types" : [ "plus_code" ]
       },
       {
        "long_name" : "Se",
        "short_name" : "Se",
        "types" : [ "administrative_area_level_4", "political" ]
       },
       {
        "long_name" : "São Paulo",
        "short_name" : "São Paulo",
        "types" : [ "administrative_area_level_2", "political" ]
       },
       {
        "long_name" : "State of São Paulo",
        "short_name" : "SP",
        "types" : [ "administrative_area_level_1", "political" ]
       },
       {
        "long_name" : "Brazil",
        "short_name" : "BR",
        "types" : [ "country", "political" ]
       }
      ],
      "formatted_address" : "F948+HX Se, São Paulo - SP, Brazil",
      "geometry" : {
       "bounds" : {
        "northeast" : {
           "lat" : -23.5435,
           "lng" : -46.6325
        },
        "southwest" : {
           "lat" : -23.543625,
           "lng" : -46.632625
        }
       },
       "location" : {
        "lat" : -23.5436238,
        "lng" : -46.6325199
       },
       "location_type" : "ROOFTOP",
       "viewport" : {
        "northeast" : {
           "lat" : -23.5422135197085,
           "lng" : -46.6312135197085
        },
        "southwest" : {
           "lat" : -23.5449114802915,
           "lng" : -46.63391148029149
        }
       }
      },
      "place_id" : "GhIJx1Pq7SqLN8AR20h-afZQR8A",
      "plus_code" : {
       "compound_code" : "F948+HX Se, São Paulo - SP, Brazil",
       "global_code" : "588MF948+HX"
      },
      "types" : [ "plus_code" ]
     }
  ],
  "status" : "OK"
 }

const Map = () => {
  Geocoder.init('AIzaSyAt8R6WxgR8wJftsycmf_Ks03tTYkREXJ4');
  const mapRef = useRef(null);

  const initialState = {
    region: null,
    destination: null,
    duration: null,
    location: null,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'setRegion':
        return {
          ...state,
          region: action.data.region,
          location: action.data.location,
        };
      case 'setDestination':
        return {
          ...state,
          destination: action.data,
        };
      default:
        break;
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const getCurrentLocation = async () => {
    Geolocation.getCurrentPosition(
      async position => {
        let {
          coords: {latitude, longitude},
        } = position;
        latitude = -23.5436238;
        longitude = -46.6325199;
        // there is a bug with this geocoder api so I`m mocking the response 
        // const response = await Geocoder.from({latitude, longitude});
        const response = geoCodingResponse;
        const address = response.results[0].formatted_address;
        const location = address.substring(0, address.indexOf(','));
        dispatch({
          type: 'setRegion',
          data: {
            region: {
              latitude,
              longitude,
              latitudeDelta: 0.0143,
              longitudeDelta: 0.0134,
            },
            location: location,
          },
        });
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const handleLocationSelected = (data, {geometry}) => {
    const {
      location: {lat: latitude, lng: longitude},
    } = geometry;
    dispatch({
      type: 'setDestination',
      data: {
        latitude,
        longitude,
        title: data.structured_formatting.main_text,
      },
    });
  };

  const handleBack = () => {
    dispatch({
      type: 'setDestination',
      data: null,
    });
  };

  const {region, destination, duration, location} = state;
  return (
    <View style={{flex: 1}}>
      <MapView
        style={{flex: 1}}
        region={region}
        showsUserLocation
        loadingEnabled
        initialCamera={{
          center: {latitude: -23.5436238, longitude: -46.6325199},
        }}
        ref={mapRef}>
        {destination && (
          <>
            <Directions
              origin={region}
              destination={destination}
              onReady={result => {
                this.setState({duration: Math.floor(result.duration)});
                mapRef.current.fitToCoordinates(result.coordinates);
              }}
            />
            <Marker
              coordinate={destination}
              anchor={{x: 0, y: 0}}
              style={{width: 100, height: 100}}
              image={markerImage}>
              <LocationBox>
                <LocationText>{destination.title}</LocationText>
              </LocationBox>
            </Marker>

            <Marker coordinate={region} anchor={{x: 0, y: 0}}>
              <LocationBox>
                <LocationTimeBox>
                  <LocationTimeText>{duration}</LocationTimeText>
                  <LocationTimeTextSmall>MIN</LocationTimeTextSmall>
                </LocationTimeBox>
                <LocationText>{location}</LocationText>
              </LocationBox>
            </Marker>
          </>
        )}
      </MapView>

      {destination ? (
        <Fragment>
          <Back onPress={handleBack}>
            <Text>Voltar</Text>
          </Back>
          <Details  from={location} to={destination.title}/>
        </Fragment>
      ) : (
        <Search onLocationSelected={handleLocationSelected} />
      )}
    </View>
  );
};

export default Map;
