import React from 'react';
import MapViewDirections from 'react-native-maps-directions';

const Directions = ({destination, origin, onReady}) => {
  return (
    <MapViewDirections
      destination={{latitude: -23.5436238, longitude: -46.6325199}}
      origin={{latitude: -23.5599169, longitude: -46.6312586}}
      onReady={onReady}
      apikey="AIzaSyCILqz_tMiaKI1-6Mj5OPzd6YNuokVgbJA"
      strokeWidth={3}
      strokeColor="#222"
      onError={errorMessage => {
        console.log('GOT AN ERROR');
      }}
    />
  );
};

export default Directions;
