import React from 'react';
import {StatusBar, StyleSheet, Text, useColorScheme} from 'react-native';
import Storybook from './storybook';
import ScreenContainer from './src/components/screen-container';
import {ThemeProvider} from 'styled-components/native';
import lightTheme from './src/configs/theme/light';
import HomeScreen from './src/screens/home'
const App = () => {
  const storybookActive = false;

  const isDarkMode = useColorScheme() === 'dark';

  if (storybookActive) {
    return <Storybook />;
  }

  return (
    <ThemeProvider theme={lightTheme}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <HomeScreen />
    </ThemeProvider>
  );
};

export default App;
